/* 
   simple_read_T_H.ino
   Example code for using the Metriful MS430 to measure humidity 
   and temperature. 
   
   Demonstrates multiple ways of reading and displaying the temperature 
   and humidity data. View the output in the Serial Monitor. The other 
   data can be measured and displayed in a similar way.
   Copyright 2020 Metriful Ltd. 
   Licensed under the MIT License - for further details see LICENSE.txt
   For code examples, datasheet and user guide, visit 
   https://github.com/metriful/sensor
*/

#include <Metriful_sensor.h>

void metrifulSetup()
{
  SensorHardwareSetup(I2C_ADDRESS);
  ready_assertion_event = false;
  TransmitI2C(I2C_ADDRESS, ON_DEMAND_MEASURE_CMD, 0, 0);

  // Now wait for the ready signal before continuing
  while (!ready_assertion_event) {
    yield();
  }
}

AirData_F_t metrifulReadAirData()
{
  return getAirDataF(I2C_ADDRESS);
}

SoundData_F_t metrifulReadSoundData()
{
  return getSoundDataF(I2C_ADDRESS);
}

LightData_F_t metrifulReadLightData()
{
  return getLightDataF(I2C_ADDRESS);
}

AirQualityData_F_t metrifulReadAirQualityData()
{
  return getAirQualityDataF(I2C_ADDRESS);
}

void metrifulReadAndPrintAllData()
{
  Serial.println("------- Metriful data -------");
  
  AirData_F_t airDataF = getAirDataF(I2C_ADDRESS);
  printAirDataF(&airDataF);
  
  Serial.println("-----------------------------");
  
  SoundData_F_t soundData = getSoundDataF(I2C_ADDRESS);
  printSoundDataF(&soundData);
  
  Serial.println("-----------------------------");
  
  LightData_F_t lightData = getLightDataF(I2C_ADDRESS);
  printLightDataF(&lightData);
  
  Serial.println("-----------------------------");
  
  AirQualityData_F_t airQualityData = getAirQualityDataF(I2C_ADDRESS);
  printAirQualityDataF(&airQualityData);

  Serial.println("-----------------------------");
}
