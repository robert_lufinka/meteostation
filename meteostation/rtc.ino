// RTC Hodiny reálného času DS3231

// připojení potřebných knihoven
#include <Wire.h>
#include <DS3231.h>
// inicializace RTC z knihovny
DS3231 rtc;
// vytvoření proměnné pro práci s časem
RTCDateTime datumCas;

void rtcSetup(bool writeDateTime)
{
  rtc.begin();

  if (writeDateTime)
  {
    rtc.setDateTime(__DATE__, __TIME__);
  }
}

RTCDateTime rtcReadData()
{
  return rtc.getDateTime();
}

void rtcReadAndPrintData()
{
  datumCas = rtc.getDateTime();

  Serial.println("------- RTC data -------");
  Serial.print(datumCas.year);   Serial.print("-");
  Serial.print(datumCas.month);  Serial.print("-");
  Serial.print(datumCas.day);    Serial.print(" ");
  Serial.print(datumCas.hour);   Serial.print(":");
  Serial.print(datumCas.minute); Serial.print(":");
  Serial.print(datumCas.second); Serial.println("");
  Serial.println("------------------------");
}
