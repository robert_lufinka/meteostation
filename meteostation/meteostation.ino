#define TIME_TO_SLEEP 60                          // sleep interval in seconds
#define WRITE_COMILATION_TIME_TO_RTC_MODULE true  // true = time of compilation is written to RTC module during every boot

static const uint8_t EPD_BUSY = 2;  // to EPD BUSY
static const uint8_t EPD_CS   = 0;  // to EPD CS
static const uint8_t EPD_RST  = 27; // to EPD RST
static const uint8_t EPD_DC   = 26; // to EPD DC
static const uint8_t EPD_SCK  = 18; // to EPD CLK
static const uint8_t EPD_MISO = 19; // Master-In Slave-Out not used, as no data from display
static const uint8_t EPD_MOSI = 23; // to EPD DIN

#include <Metriful_sensor.h>
#include <DS3231.h>
#include <esp_deep_sleep.h>

// base class GxEPD2_GFX can be used to pass references or pointers to the display instance as parameter, uses ~1.2k more code
// enable or disable GxEPD2_GFX base class
#define ENABLE_GxEPD2_GFX 0

#include <GxEPD2_BW.h>
/*
#include <Fonts/FreeMonoBold20pt7b.h>
#include <Fonts/FreeMonoBold40pt7b.h>
#include <Fonts/FreeMonoBold50pt7b.h>
*/
#include <Fonts/FreeSans20pt7b.h>
#include <Fonts/FreeSansBold20pt7b.h>
#include <Fonts/FreeSansBold40pt7b.h>
#include <Fonts/FreeSansBold50pt7b.h>

GxEPD2_BW<GxEPD2_750_T7, GxEPD2_750_T7::HEIGHT> display(GxEPD2_750_T7(/*CS=*/ EPD_CS, /*DC=*/ EPD_DC, /*RST=*/ EPD_RST, /*BUSY=*/ EPD_BUSY));

void setup()
{
  metrifulSetup();
  rtcSetup(false);

  display.init(0, true, 2);
  SPI.end();
  SPI.begin(EPD_SCK, EPD_MISO, EPD_MOSI, EPD_CS);

  displayData();
  deepSleep();
}

void loop()
{
  // we never get here due to dee sleep inituialted in setup function
}

void deepSleep() {
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * 1000000);
  esp_deep_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
  esp_deep_sleep_start();
}

void displayData()
{    
  char text[] = "                                                  ";

  // ---------- Metriful data ----------

  /*
  float    T_C;
  uint32_t P_Pa;
  float    H_pc;
  uint32_t G_Ohm;
  */
  AirData_F_t airData = metrifulReadAirData();

  /*
  float SPL_dBA;
  float SPL_bands_dB[SOUND_FREQ_BANDS];
  float peakAmp_mPa;
  bool  stable;
   */
  // SoundData_F_t soundData = metrifulReadSoundData();

  /*
  float    illum_lux;
  uint16_t white;
   */
  // LightData_F_t loghtData = metrifulReadLightData();

  /*
  float   AQI;
  float   CO2e;
  float   bVOC;
  uint8_t AQI_accuracy;
   */
  char * airQualityStr;
  AirQualityData_F_t airQData = metrifulReadAirQualityData();

  if (airQData.AQI < 50) {
    airQualityStr = "dobra";
  }
  else if (airQData.AQI < 100) {
    airQualityStr = "akceptovatelna";
  }
  else if (airQData.AQI < 150) {
    airQualityStr = "standard";
  }
  else if (airQData.AQI < 200) {
    airQualityStr = "slaba";
  }
  else if (airQData.AQI < 300) {
    airQualityStr = "spatna";
  }
  else {
    airQualityStr = "velmi spatna";
  }   

  // ---------- RTC data ----------

  RTCDateTime rtcData = rtcReadData();

  // ---------- Display data ----------

  display.setRotation(0);  
  display.setTextColor(GxEPD_BLACK);
  display.setFullWindow();
  display.fillScreen(GxEPD_WHITE);

  display.setFont(&FreeSansBold40pt7b);

  sprintf(text, "%02d.%02d.%d   %02d:%02d\0", rtcData.day, rtcData.month, rtcData.year, rtcData.hour, rtcData.minute);
  display.drawRect(5, 5, 790, 156, GxEPD_BLACK);
  drawText(text, 5, 5, 790, 156);

  display.setFont(&FreeSansBold50pt7b);

  sprintf(text, "%.1f C\0", airData.T_C);
  display.drawRect(5, 160, 396, 240, GxEPD_BLACK);
  drawText(text, 5, 220, 395, 180);

  sprintf(text, "%.1f %%\0", airData.H_pc);
  display.drawRect(400, 160, 395, 240, GxEPD_BLACK);
  drawText(text, 400, 220, 395, 180);

  display.setFont(&FreeSans20pt7b);
  
  drawText("Teplota:", 5, 160, 395, 60);
  drawText("Vlhkost:", 400, 160, 395, 60);

  display.setFont(&FreeSansBold20pt7b);
    
  sprintf(text, "Kvalita vzduchu:   %s\0", airQualityStr);
  display.drawRect(5, 399, 790, 80, GxEPD_BLACK);
  drawText(text, 5, 399, 790, 80);
  
  display.display(false);
  display.powerOff();
}

void drawText(char * text, int x, int y, int w, int h)
{
  int16_t tbx, tby; uint16_t tbw, tbh;
  display.getTextBounds(text, 0, 0, &tbx, &tby, &tbw, &tbh);

  x = ((w - tbw) / 2) + x;
  y = ((h - tbh) / 2) + y + tbh;

  display.setCursor(x, y);
  display.print(text);    
}
